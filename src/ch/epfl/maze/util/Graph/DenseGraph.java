package ch.epfl.maze.util.Graph;

import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.World;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

public class DenseGraph extends Graph {
	public DenseGraph(Daedalus daedalus) {
		super(daedalus);
	}

	@Override
	public void parseDaedalus(Daedalus daedalus) {
		// Revoke super constructor
		super.parseDaedalus(null);

		if (daedalus == null)
			return;

		for (int x = 0; x < daedalus.getWidth(); ++x)
			for (int y = 0; y < daedalus.getHeight(); ++y) {
				if (daedalus.getTile(x, y) != World.FREE)
					continue;
				Vector2D currPosition = new Vector2D(x, y);
				graphNodes.add(new Node(currPosition));
			}
		linkNodes(daedalus);
	}

	/**
	 * This function link Nodes with their neighbors note that all the nodes
	 * in the graph must have been registered prior to a call to this function
	 * @see parseDaedalus
	 * 
	 * @param daedalus
	 *  	daedalus to parse from
	 * @param nodePosition
	 *  	desiredNodePosition
	 * @param existingNodes
	 *  	arrayList contained known nodes (will add new nodes to it)
	 */
	private void linkNodes(Daedalus daedalus) {
		for (Node node : graphNodes) {
			Vector2D nodePosition = node.getPosition();
			Direction[] nodeLinks = daedalus.getChoices(nodePosition);
			for (Direction nodeLink : nodeLinks) {
				try {
					node.addFriend(
						graphNodes.get(
							graphNodes.indexOf(
								new Node(nodePosition.addDirectionTo(nodeLink)))),1);

				} catch (ArrayIndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					// This should not arrive and indicated an error in the
					// nodes mapper
					throw new RuntimeException("DenseGraph nodes linker failed, found friend on non-existent node: friend@"
							+ nodePosition.addDirectionTo(nodeLink) + " from node@" 
							+ node.getPosition() + " on direction"
							+ nodeLink);
				}
			}
		}
	}
}
