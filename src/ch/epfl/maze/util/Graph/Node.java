package ch.epfl.maze.util.Graph;

import java.util.ArrayList;

import ch.epfl.maze.util.Vector2D;

public class Node {
	private Vector2D 				pos;
	private ArrayList<Node>		friendNodes	= new ArrayList<Node>();
	private ArrayList<Integer>	edgeLengths	= new ArrayList<Integer>();

	/**
	 * Node constructor
	 * @param x
	 * 		node 'x' position/ID[0]
	 * @param y
	 * 		node 'y' position/ID[1]
	 */
	public Node(int x, int y) {
		pos = new Vector2D(x,y);
	}

	/**
	 * Node constructor
	 * @param id
	 * 		node ID array of size at least two (usually it's position)
	 */
	public Node(Vector2D vect) {
		pos = new Vector2D(vect);
	}
	
	/**
	 * Adds a connected node
	 * @param friend
	 * 		neighbor node
	 * @param distance
	 * 		distance to that node
	 */
	public void addFriend(Node friend, int distance) {
		if (friend == null || distance < 0)
			throw new RuntimeException("IAX: Invalid Friend!");
		if (friendNodes.contains(friend)) {
			edgeLengths.set(friendNodes.indexOf(friend), new Integer(distance));
		} else {
			friendNodes.add(friend);
			edgeLengths.add(new Integer(distance));
		}
	}
	
	/**
	 * Returns an array list to it's friends
	 * @return
	 */
	public ArrayList<Node> getFriends(){
		return friendNodes;
		//return new ArrayList<Node>(friendNodes);
	}
	
	/**
	 * Returns an array list to it's friend's distance
	 * @return
	 */
	public ArrayList<Integer> getEdgeLengths(){
		return edgeLengths;
		//return new ArrayList<Integer>(edgeLengths);
	}

	/**
	 * Returns node position in daedalus
	 * @return
	 */
	public Vector2D getPosition(){
		return new Vector2D(pos);
	}

	/**
	 * So that two nodes in the same position are "equal"
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Node))
			return false;

		return ((Node) obj).pos.equals(pos);
	}
}
