package ch.epfl.maze.util.Graph;

/**
 * This is just for debugging purposes, remove later...
 * @author roosemberth
 *
 */
public class NoFriendFoundException extends Exception {
	private static final long serialVersionUID = -8022953229990980487L;
}
