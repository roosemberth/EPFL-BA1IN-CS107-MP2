package ch.epfl.maze.util.Graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.World;
import ch.epfl.maze.physical.pacman.Ant;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

public class Graph {
	protected ArrayList<Node> graphNodes = new ArrayList<Node>();

	/**
	 * Construct a Graph from a Daedalus
	 * 
	 * @param daedalus
	 */
	public Graph(Daedalus daedalus) {
		parseDaedalus(daedalus);
	}

	/**
	 * Copy Constructor
	 * 
	 * @param nodes
	 */
	public Graph(ArrayList<Node> nodes) {
		if (nodes != null)
			this.graphNodes = new ArrayList<Node>(nodes);
		else
			this.graphNodes = new ArrayList<Node>();
	}
	
	/**
	 * Convert a Daedalus into a Node Array,
	 * this call will replace the current set of nodes
	 * 
	 * @param daedalus
	 *  	daedalus to convert
	 * @return array of graph nodes, null if a null daedalus was given
	 * 
	 */
	public void parseDaedalus(Daedalus daedalus) {
		if (daedalus == null)
			return;

		for (int x = 0; x < daedalus.getWidth(); ++x)
			for (int y = 0; y < daedalus.getHeight(); ++y) {
				if (daedalus.getTile(x, y) != World.FREE)
					continue;
				Vector2D currPosition = new Vector2D(x, y);
				if (daedalus.getChoices(currPosition).length > 2)
					graphNodes.add(new Node(currPosition));
			}
		linkNodes(daedalus);
	}

	/**
	 * This function link Nodes with their neighbors note that all the nodes
	 * in the graph must have been registered prior to a call to this function
	 * @see parseDaedalus
	 * 
	 * @param daedalus
	 *  	daedalus to parse from
	 * @param nodePosition
	 *  	desiredNodePosition
	 * @param existingNodes
	 *  	arrayList contained known nodes (will add new nodes to it)
	 */
	private void linkNodes(Daedalus daedalus) {
		for (Node node : graphNodes) {
			Vector2D nodePosition = node.getPosition();
			Direction[] nodeLinks = daedalus.getChoices(nodePosition);
			for (Direction nodeLink : nodeLinks) {
				try {
					FriendLink friendLink = new FriendLink(daedalus, nodePosition, nodeLink);
					Node friend;
					try {
						Vector2D friendPosition = friendLink.getFriendPosition();
						if (friendPosition == null)
							continue;
						friend = graphNodes.get(
								graphNodes.indexOf(
										new Node(friendPosition)));
					} catch (ArrayIndexOutOfBoundsException e) {
						// This should not arrive and indicated an error in the nodes mapper
						throw new RuntimeException(
								"Friend Link Tracker failed, found friend on non-existent node: friend@" 
									+ friendLink.getFriendPosition() + " from node@" 
									+ node.getPosition() + " on direction"
									+ nodeLink);
					}
					if (friend!=node && friendLink.getFriendPosition()!=null)
						node.addFriend(friend, friendLink.getLinkLength());
				} catch (NoFriendFoundException e) {
					// friend not found, just do nothing
				}
			}
		}
	}
	
	/**
	 * Calculate a route for the first source so that the other sources don't 
	 * intercept it's way
	 * 
	 * @param sources
	 * @return
	 */
	public Direction[] maxPrimerFlow(Vector2D[] sources, Daedalus daedalus){
		Vector2D origin = sources[0];
		ArrayList<Vector2D> allSources = new ArrayList<Vector2D>(Arrays.asList(sources));
		allSources.remove(sources[0]);
		Vector2D[] otherSources = allSources.toArray(new Vector2D[allSources.size()]);

		Ant explorer = new Ant(origin, otherSources, daedalus);
		explorer.run();
		//ArrayList<Vector2D> antHistory = new ArrayList<>(explorer.getHistory());
		//Vector2D[] maxPrimerFlowPath = 
		//		explorer.getHistory().toArray(new Vector2D[explorer.getAge()]);
		Vector2D[] maxPrimerFlowPath = 
				explorer.getHistory().toArray(new Vector2D[explorer.getAge()]);
		ArrayList<Direction> maxPrimerFlowDirections = new ArrayList<Direction>();
		for (int i = 0; i < (maxPrimerFlowPath.length - 1); ++i) {
			Vector2D stepStart = maxPrimerFlowPath[i];
			if (stepStart==null)
				continue;
			Vector2D stepEnd = maxPrimerFlowPath[i+1];
			if (stepEnd == null)
				continue;
//			Direction nextDir = 
//					maxPrimerFlowPath[i+1].sub(maxPrimerFlowPath[i]).toDirection();
			Direction nextDir = stepEnd.sub(stepStart).toDirection();
			if (nextDir==Direction.NONE)
				continue;
			maxPrimerFlowDirections.add(nextDir);
		}
		
		return maxPrimerFlowDirections.toArray(new Direction[maxPrimerFlowDirections.size()]);
	}
	
	/**
	 * Calculates best path between two nodes in an undirected graph
	 * @param from node
	 * @param to node
	 * @return
	 * 		ArrayList containing the best path nodes
	 */
	public ArrayList<Node> dijkstra(Vector2D from, Vector2D to){
		ArrayList<Node> minPath = new ArrayList<Node>();
		ArrayList<Node> discoveredNodes = new ArrayList<Node>();
		ArrayList<Node> bestPredecessor = new ArrayList<Node>();
		ArrayList<Integer> minDistance = new ArrayList<Integer>();
		
		Node sourceNode = graphNodes.get(graphNodes.indexOf(new Node(from)));
		Node sinkNode = graphNodes.get(graphNodes.indexOf(new Node(to)));
		
		discoveredNodes.add(sourceNode);
		bestPredecessor.add(sourceNode);
		minDistance.add(0);

		int lastIterationIndex = 0;
		int thisIterationIndex = discoveredNodes.size();
		do {
			for (int i = lastIterationIndex; i<thisIterationIndex; ++i){
				Node currNode = discoveredNodes.get(i);
				int currNodeDistance = minDistance.get(i);
				ArrayList<Node> currNodeFriends = currNode.getFriends();
				ArrayList<Integer> edgeLengths = currNode.getEdgeLengths();
				for (int j = 0; j < currNodeFriends.size(); ++j){
					Node friend = currNodeFriends.get(j);
					// Avoid loops
					if (friend.equals(currNode))
						continue;
					int friendDistance = edgeLengths.get(j);
					if (discoveredNodes.contains(friend)){
						int k = discoveredNodes.indexOf(friend);
						int distanceToThisNode = currNodeDistance + friendDistance;
						if (minDistance.get(k) > distanceToThisNode) {
							minDistance.set(k, distanceToThisNode);
							bestPredecessor.set(k, currNode);
						}
					} else {
						discoveredNodes.add(friend);
						bestPredecessor.add(currNode);
						minDistance.add(currNodeDistance + friendDistance);
					}
				}
			}
			lastIterationIndex = thisIterationIndex;
			thisIterationIndex = discoveredNodes.size();
		} while (discoveredNodes.size()<graphNodes.size());
		
		Node currNode = sinkNode;
		do {
			minPath.add(currNode);
			currNode = bestPredecessor.get(discoveredNodes.indexOf(currNode));
		} while (!minPath.get(minPath.size()-1).equals(sourceNode));
		Collections.reverse(minPath);
		if (minPath.get(0)!=sourceNode || minPath.get(minPath.size()-1)!=sinkNode)
			throw new RuntimeException("Dijksta failed...");
		
		return minPath;
	}
}
