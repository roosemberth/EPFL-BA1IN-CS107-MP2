package ch.epfl.maze.util.Graph;

import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

public class FriendLink {
	private Vector2D	friendPosition;
	private int		linkLength;

	public Vector2D getFriendPosition() {
		if (friendPosition==null)
			return null;
		return new Vector2D(friendPosition);
	}

	public int getLinkLength() {
		return linkLength;
	}

	/**
	 * Find the friend node in the corresponding description is there's no friend,
	 * 
	 * @param daedalus
	 *            Daedalus to parse from
	 * @param startPosition
	 *            Follow path from
	 * @param followDirection
	 *            Follow path from in this direction
	 */
	public FriendLink(Daedalus daedalus, Vector2D startPosition, Direction followDirection) 
			throws NoFriendFoundException {
		Vector2D linkFollowerPosition = new Vector2D(startPosition);
		Direction ahead = followDirection;
		Direction behind = followDirection.reverse();
		Direction[] pathOptions = { ahead, behind };
		int pathLenght = 0;

		do {
			// We enter here under the asumption that pathOptions length is 2
			++pathLenght;
			ahead = pathOptions[0];
			if (ahead == behind)
				ahead = pathOptions[1];
			behind = ahead.reverse();
			// Give the step
			linkFollowerPosition = linkFollowerPosition.add(ahead.toVector());
			pathOptions = daedalus.getChoices(linkFollowerPosition);
		} while (pathOptions.length == 2);

		// FIXME: Consider is lathOptions.length can be one in any case...
		if (pathOptions.length == 1) {
			// No friend here, dead end
			friendPosition = null;
			return;
		}
		// We found a friend :D
		friendPosition = new Vector2D(linkFollowerPosition);
		linkLength 	= pathLenght;
	}
}
