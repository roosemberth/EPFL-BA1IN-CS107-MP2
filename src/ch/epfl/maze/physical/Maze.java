package ch.epfl.maze.physical;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roosembert Palacios
 * @author Thomas Adam
 * 
 *         Maze in which an animal starts from a starting point and must find
 *         the exit. Every animal added will have its position set to the
 *         starting point. The animal is removed from the maze when it finds the
 *         exit.
 * 
 */

public final class Maze extends World {

	/**
	 * Constructs a Maze with a labyrinth structure.
	 * 
	 * @param labyrinth
	 *            Structure of the labyrinth, an NxM array of tiles
	 */

	// animalsIn is the list of animals present in the labyrinth animalsOut is
	// the list of animals that were removed from labyrinth
	private List<Animal> animalsIn = new ArrayList<Animal>();
	private List<Animal> animalsOut = new ArrayList<Animal>();

	public Maze(int[][] labyrinth) {
		super(labyrinth);
	}

	@Override
	public boolean isSolved() {
		return (animalsIn.size() == 0);
	}

	@Override
	public List<Animal> getAnimals() {
		// As asked in the project description, we allow ourselves to directly
		// return animalsIn instead of a copy
		return animalsIn;
	}

	/**
	 * Determines if the maze contains an animal.
	 * 
	 * @param a
	 *            The animal in question
	 * @return <b>true</b> if the animal belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasAnimal(Animal a) {
		return (animalsIn.indexOf(a) != -1);
	}

	/**
	 * Adds an animal to the maze.
	 * 
	 * @param a
	 *            The animal to add
	 */

	public void addAnimal(Animal a) {
		// Put animal at start position
		a.setPosition(getStart());
		// Then add it to the list of active animals
		animalsIn.add(a);
	}

	/**
	 * Removes an animal from the maze.
	 * 
	 * @param a
	 *            The animal to remove
	 */

	public void removeAnimal(Animal a) {
		// Put animal to the list of old animals
		animalsOut.add(animalsIn.get(animalsIn.indexOf(a)));
		// Then remove it from list of active animals
		animalsIn.remove(a);
	}

	@Override
	public void reset() {
		// First put all the active animals to the list of old animals
		for (int i = 0; i < animalsIn.size(); ++i) {
			animalsOut.add(animalsIn.get(i));
		}
		// Then remove all the animals from list of active animals
		animalsIn.clear();
		// Then add a copy of old animals at the start
		for (int i = 0; i < animalsOut.size(); ++i) {
			addAnimal(animalsOut.get(i).copy());
		}
		// Then remove all animals from the list of old animals
		animalsOut.clear();
	}
}
