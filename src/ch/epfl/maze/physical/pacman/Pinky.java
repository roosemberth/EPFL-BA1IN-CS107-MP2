package ch.epfl.maze.physical.pacman;

import java.util.ArrayList;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Pink ghost from the Pac-Man game, targets 4 squares in front of its target.
 * 
 */

public class Pinky extends Predator {
	private Vector2D	home 			= null;
	private Prey		prey			= null;
	private Vector2D 	targetPos		= null;
	private Vector2D 	targetLastPos 	= null;
	private Mode		cMode			= Mode.CHASE;
	private int		stepsInCurrMode	= 0;

	/**
	 * Constructs a Pinky with a starting position.
	 * 
	 * @param position
	 *            Starting position of Pinky in the labyrinth
	 */
	private final Direction prefferedDirection = Direction.UP;

	public Pinky(Vector2D position) {
		super(position);
		Vector2D pos = new Vector2D(position.getX(), position.getY());
		home = pos;
	}

	private Direction moveChase(Direction[] choices) {
		Vector2D displacement = targetPos.sub(targetLastPos);
		Vector2D target = targetPos.add(displacement.mul(4));
		targetLastPos = targetPos;
		return super.hunt(choices, getPosition(), target);
	}

	private Direction moveScatter(Direction[] choices) {
		targetLastPos = null;
		return super.hunt(choices, getPosition(), home);
	}

	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		++stepsInCurrMode;
		if (cMode == Mode.CHASE && stepsInCurrMode > CHASE_DURATION) {
			cMode = Mode.SCATTER;
			stepsInCurrMode = 0;
		} else if (cMode == Mode.SCATTER && stepsInCurrMode > SCATTER_DURATION) {
			cMode = Mode.CHASE;
			stepsInCurrMode = 0;
		}

		if (cMode == Mode.SCATTER){
			if (getPosition().equals(home)){
				cMode = Mode.CHASE;
				stepsInCurrMode = 0;
			}
			return moveScatter(choices);
		}

		/*
		 * Not in Scatter mode, therefore in Chase mode...
		 */
		if (prey == null || !daedalus.hasPrey(prey)){
			ArrayList<Animal> preys = new ArrayList<Animal>(daedalus.getPreys());
			prey = (Prey) findNearestAnimal(preys, this.getPosition());
			// no preys in the map, move randomly...
			if (prey == null)
				return super.move(choices);
		}
		
		targetPos = prey.getPosition();
		if (targetLastPos == null)
			targetLastPos = targetPos.sub(prefferedDirection.toVector());
		
		return moveChase(choices);
	}

	@Override
	public Animal copy() {
		return new Pinky(new Vector2D(getPosition().getX(), getPosition().getY()));
	}
}
