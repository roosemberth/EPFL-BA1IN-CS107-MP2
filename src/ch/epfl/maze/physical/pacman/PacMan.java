package ch.epfl.maze.physical.pacman;

import java.util.ArrayList;
import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;
import ch.epfl.maze.util.Graph.Graph;

/**
 * Pac-Man character, from the famous game of the same name.
 * 
 */

public class PacMan extends Prey {
	private static final int INSTRUCTION_CACHE_SIZE = 0;
	private ArrayList<Direction> directionCache = new ArrayList<Direction>();
	public PacMan(Vector2D position) {
		super(position);
	}

	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		// return super.move(choices);
		Graph graph = new Graph(daedalus);
		return runFromPredators(daedalus, graph);
	}

	@Override
	public Animal copy() {
		return new PacMan(new Vector2D(getPosition().getX(), getPosition().getY()));
	}

	/**
	 * Get the Direction of the best escape route
	 * 
	 * @param daedalus
	 * @param graph
	 * @return
	 */
	private Direction runFromPredators(Daedalus daedalus, Graph graph) {
		if (!directionCache.isEmpty()){
			Direction ret = directionCache.get(0);
			directionCache.remove(ret);
			return ret;
		}
		List<Predator> predators = daedalus.getPredators();
		Vector2D[] sources = new Vector2D[predators.size() + 1];
		for (Predator pred : predators) {
			sources[1 + predators.indexOf(pred)] = pred.getPosition();
		}
		List<Prey> preys = daedalus.getPreys();
		int pacmanIndex = preys.indexOf(this);
		if (pacmanIndex < 0){
			System.err.println("PACMAN: I can't find myself on the daedalus...");
			if (preys.size() == 1){
				System.err.println("I found out that there's only one prey, that must be me...");
				pacmanIndex = 0;
			} else {
				System.err.println("I dont know to to find me..., I give up");
				throw new RuntimeException("Pacman could not be located on daedalus...");
			}
		}
		sources[0] = preys.get(pacmanIndex).getPosition();
		
		Direction[] escapePlan = graph.maxPrimerFlow(sources, daedalus);
		
		int cacheElements = INSTRUCTION_CACHE_SIZE + 1;
		if (escapePlan.length<cacheElements)
			cacheElements = escapePlan.length;
		
		for (int i = 0; i<cacheElements; ++i){
			directionCache.add(escapePlan[i]);
		}
		
		if (!directionCache.isEmpty()){
			Direction ret = directionCache.get(0);
			directionCache.remove(ret);
			return ret;
		}

		return Direction.NONE;
	}
}