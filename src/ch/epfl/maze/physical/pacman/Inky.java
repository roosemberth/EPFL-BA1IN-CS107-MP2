package ch.epfl.maze.physical.pacman;

import java.util.ArrayList;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Blue ghost from the Pac-Man game, targets the result of two times the vector
 * from Blinky to its target.
 * 
 */

public class Inky extends Predator {
	private Vector2D	home 			= null;
	private Prey		prey			= null;
	private Vector2D 	targetPos		= null;
	private Mode		cMode			= Mode.CHASE;
	private int		stepsInCurrMode	= 0;
	private Blinky 	blinky			= null;

	/**
	 * Constructs a Inky with a starting position.
	 * 
	 * @param position
	 *            Starting position of Inky in the labyrinth
	 */

	public Inky(Vector2D position) {
		super(position);
		home = new Vector2D(position.getX(), position.getY());
	}

	private Direction moveChase(Direction[] choices) {
		// target = (OC); blinky = (OA); prey = (OB); me = (OP)
		// (OC) = 2(OB)-(OA) <=> (PC) = (OC)-(OP) = 2(OB)-(OA)-(OP)
		Vector2D target = 
				targetPos.mul(2).
				  sub(blinky.getPosition()).
				  	sub(this.getPosition());
		return super.hunt(choices, getPosition(), target);
	}

	private Direction moveScatter(Direction[] choices) {
		return super.hunt(choices, getPosition(), home);
	}

	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		++stepsInCurrMode;
		if (cMode == Mode.CHASE && stepsInCurrMode > CHASE_DURATION) {
			cMode = Mode.SCATTER;
			stepsInCurrMode = 0;
		} else if (cMode == Mode.SCATTER && stepsInCurrMode > SCATTER_DURATION) {
			cMode = Mode.CHASE;
			stepsInCurrMode = 0;
		}

		if (cMode == Mode.SCATTER){
			if (getPosition().equals(home)){
				cMode = Mode.CHASE;
				stepsInCurrMode = 0;
			}
			return moveScatter(choices);
		}

		/*
		 * Not in Scatter mode, therefore in Chase mode...
		 */
		if (prey == null || !daedalus.hasPrey(prey)){
			ArrayList<Animal> preys = new ArrayList<Animal>(daedalus.getPreys());
			prey = (Prey) findNearestAnimal(preys, this.getPosition());
			// no preys in the map, move randomly...
			if (prey == null)
				return super.move(choices);
		}
		
		for (Predator pred : daedalus.getPredators()){
			if (pred.getClass().getName()==Blinky.class.getName()){
				blinky = (Blinky)pred;
				break;
			}
		}
		if (blinky == null){
			// We can't find blinky ;(
			throw new RuntimeException("Couldn't find blinky! :'(");
			// Fine, I'll do some fallback code, just comment the line abode and bellow me...
			/* Add '//' at the beguining of this line
			return super.hunt(choices, getPosition(), prey.getPosition());
			// */
		}
		
		targetPos = prey.getPosition();

		return moveChase(choices);
	}

	@Override
	public Animal copy() {
		return new Inky(new Vector2D(getPosition().getX(), getPosition().getY()));
	}
}
