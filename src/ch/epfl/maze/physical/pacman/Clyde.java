package ch.epfl.maze.physical.pacman;

import java.util.ArrayList;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Orange ghost from the Pac-Man game, alternates between direct chase if far
 * from its target and SCATTER if close.
 * 
 */

public class Clyde extends Predator {
	private Vector2D	home 			= null;
	private Prey		prey			= null;
	private Vector2D 	targetPos		= null;
	private Mode		cMode			= Mode.CHASE;
	private int		stepsInCurrMode	= 0;
	private boolean 	naturaleza 		= false;

	/**
	 * Constructs a Clyde with a starting position.
	 * 
	 * @param position
	 *            Starting position of Clyde in the labyrinth
	 */

	public Clyde(Vector2D position) {
		super(position);
		home = new Vector2D(position.getX(), position.getY());
	}

	private Direction moveChase(Direction[] choices) {
		Vector2D target = targetPos;

		if (distanceToAnimal(prey, getPosition())<4)
			naturaleza = true;
		if (getPosition().equals(home))
			naturaleza = false;
		if (naturaleza)
			return moveScatter(choices);

		return super.hunt(choices, getPosition(), target);
	}

	private Direction moveScatter(Direction[] choices) {
		return super.hunt(choices, getPosition(), home);
	}

	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		++stepsInCurrMode;
		if (cMode == Mode.CHASE && stepsInCurrMode > CHASE_DURATION) {
			naturaleza = false;
			cMode = Mode.SCATTER;
			stepsInCurrMode = 0;
		} else if (cMode == Mode.SCATTER && stepsInCurrMode > SCATTER_DURATION) {
			naturaleza = false;
			cMode = Mode.CHASE;
			stepsInCurrMode = 0;
		}

		if (cMode == Mode.SCATTER){
			if (getPosition().equals(home)){
				cMode = Mode.CHASE;
				stepsInCurrMode = 0;
			}
			return moveScatter(choices);
		}

		/*
		 * Not in Scatter mode, therefore in Chase mode...
		 */
		if (prey == null || !daedalus.hasPrey(prey)){
			ArrayList<Animal> preys = new ArrayList<Animal>(daedalus.getPreys());
			prey = (Prey) findNearestAnimal(preys, this.getPosition());
			// no preys in the map, move randomly...
			if (prey == null)
				return super.move(choices);
		}
		
		targetPos = prey.getPosition();

		return moveChase(choices);
	}

	@Override
	public Animal copy() {
		return new Clyde(new Vector2D(getPosition().getX(), getPosition().getY()));
	}
}
