package ch.epfl.maze.physical.pacman;

import java.util.ArrayList;
import java.util.Arrays;

import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;
import ch.epfl.maze.util.Graph.DenseGraph;
import ch.epfl.maze.util.Graph.Node;

public class Ant extends Thread {
	private static final int FORK_LEVEL_LIMIT = 4;
	private int forkLevel = 0;
	private Vector2D[] otherSources;
	private Daedalus daedalus;
	Direction ahead;
	Direction behind;

	private ArrayList<Vector2D> history = new ArrayList<Vector2D>();
	DenseGraph denseGraph;
	private Vector2D antPosition;
	private boolean dead;
	private int age = 0;

	/**
	 * Ant travel record
	 * @return
	 */
	public ArrayList<Vector2D> getHistory() {
		return history;
	}

	/**
	 * Ant life status
	 * @return
	 *  	true if the ant is dead (AKA data ready)
	 */
	public boolean isDead() {
		return dead;
	}
	
	public void kill(){
		dead = true;
	}

	/**
	 * Get an ant's age
	 * @return
	 *  	the legth of the path traveled
	 */
	public int getAge() {
		return age;
	}
	
	private boolean originAnt = false;

	/**
	 * Construct an ant
	 * @param position
	 * @param otherSources
	 * @param daedalus
	 */
	public Ant(Vector2D position, Vector2D[] otherSources, Daedalus daedalus) {
		antPosition = new Vector2D(position);
		this.otherSources = otherSources;
		this.daedalus = daedalus;
		history = new ArrayList<Vector2D>();
		history.add(position);
		age = history.size();
		originAnt = true;
		denseGraph = new DenseGraph(daedalus);
	}

	/**
	 * Construct an ant with a past, used when forking an ant
	 * @param past
	 *  	history of ancient positions, will die if it goes back again there
	 * @param followDirection
	 *  	where the ant should head to
	 * @param otherSources
	 *  	flow sources in the ant's world
	 * @param daedalus
	 *  	is the ant's world
	 */
	public Ant(ArrayList<Vector2D> past, Direction followDirection, Vector2D[] otherSources, Daedalus daedalus, int forkLevel) {
		this.forkLevel = forkLevel + 1;
		antPosition = new Vector2D(past.get(past.size()-1));
		this.otherSources = otherSources;
		this.daedalus = daedalus;
		history = new ArrayList<Vector2D>(past);
		ahead = followDirection;
		behind = followDirection.reverse();
		age = history.size();
		denseGraph = new DenseGraph(daedalus);
	}

	/**
	 * Run the ant
	 * 
	 * An ant will follow the path from its position ahead until reaching a dead
	 * end, a self crossing path an intersection (in which case will spawn a new
	 * ant on each direction) or until crossing another source's flow path (ie. 
	 * distance from the origin is less than the distance no another source). 
	 * Note that "distance" refer to "walking distance" and not euclidean 
	 * distance.
	 */
	@Override
	public void run() {
		super.run();
		if (originAnt){
			fork(daedalus.getChoices(antPosition));
			dead = true;
			return;
		}
		if (forkLevel>FORK_LEVEL_LIMIT || dead){
			dead = true;
			return;
		}

		Direction[] pathOptions = { ahead, behind };

		int distanceToOtherNearestSource;

		do {
			++age;
			ahead = pathOptions[0];
			if (ahead == behind)
				ahead = pathOptions[1];
			behind = ahead.reverse();
			// Give the step
			antPosition = antPosition.add(ahead.toVector());
			pathOptions = daedalus.getChoices(antPosition);
			distanceToOtherNearestSource = distanceToNearestSource();
			if (history.contains(antPosition))
				break;
			history.add(antPosition);
		} while (pathOptions.length == 2 
				&& distanceToOtherNearestSource > age);

		if (history.indexOf(antPosition)!=history.size()-1) {
			// Self-Intersecting path
			dead = true;
			return;
		}

		if (distanceToOtherNearestSource <= age) {
			// In range of a sourceflow
			dead = true;
			return;
		}

		if (pathOptions.length == 1) {
			// DEAD END?
			// Especially don't come here, I'll change my age so the parent ant
			// knows this path is a bad idea
			age = 1;
			dead = true;
			return;
		}
		
		// More than 3 options, new branching...
		if (pathOptions.length>2){
			// Spawn a new set of ants
			// Create next branches but avoid to branch to backwards
			ArrayList<Direction> branches = new ArrayList<Direction>(Arrays.asList(daedalus.getChoices(antPosition)));
			branches.remove(behind);
			// Fork and wait for return... 
			fork(branches.toArray(new Direction[branches.size()]));
		}
		// (yes I know it's not necessary to explicit return...)
		dead = true;
		return;
	};
	
	/**
	 * Fork this ant and let its descendants find the best way...
	 * @param branches
	 * 		a directions array for each future ant
	 */
	private void fork(Direction[] branches){
		Ant[] futureAnts = new Ant[branches.length];

		for (int i = 0; i < branches.length; ++i) {
			Direction branch = branches[i];
			futureAnts[i] = new Ant(history, branch, otherSources, daedalus, forkLevel);
			futureAnts[i].run();
		}

		for (Ant futureAnt : futureAnts) {
				try {
					// Wait 10 ms
					futureAnt.join(10);
				} catch (InterruptedException e) {
					// Do nothing
				}
			while (!futureAnt.isDead()){
				futureAnt.kill();
			}
			if (futureAnt.age > age) {
				age = futureAnt.age;
				history = futureAnt.history;
			}
		}
	}
	
	/**
	 * Calculate the distance to the nearest source
	 * @return
	 *  	Distance to the nearest source
	 */
	public int distanceToNearestSource(){
		int minDistance = Integer.MAX_VALUE;
		if (otherSources == null || otherSources.length == 0)
			return minDistance;
		
		for (Vector2D source : otherSources){
			int dist = minPathLength(antPosition, source);
			if (dist < minDistance)
				minDistance = dist;
		}
		
		return minDistance;
	}
	
	/**
	 * Calculate path length
	 * @param from
	 *  	Start Point
	 * @param to
	 *  	Target Point
	 * @return
	 *  	Minimum path distance
	 */
	public int minPathLength(Vector2D from, Vector2D to){
		ArrayList<Node> minLength = denseGraph.dijkstra(from, to);
		return minLength.size();
		//return 2*(int)to.sub(from).dist();
	}
}
