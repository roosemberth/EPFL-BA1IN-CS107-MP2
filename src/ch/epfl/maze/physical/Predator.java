package ch.epfl.maze.physical;

import java.util.List;
import java.util.Random;

import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Predator that kills a prey when they meet with each other in the labyrinth.
 * 
 */

abstract public class Predator extends Animal {

	/* constants relative to the Pac-Man game */
	public static final int SCATTER_DURATION = 14;
	public static final int CHASE_DURATION = 40;
	public enum Mode{
		CHASE, SCATTER
	}
	
	/**
	 * Constructs a predator with a specified position.
	 * 
	 * @param position
	 *            Position of the predator in the labyrinth
	 */

	public Predator(Vector2D position) {
		super(position);
	}
	
	/**
	 * "A predator knows how to hunt by instinct"
	 * Picks the choice with reduces the euclidean distance from the predator 
	 * to the target
	 * 
	 * @param choices	Choices of the Predator
	 * @param from		Where the Predator is
	 * @param to		Where the target is
	 * @return Direction on which It'll go
	 */
	public static Direction hunt(Direction[] choices, Vector2D from, Vector2D to){
		double distanceToTarget = Double.MAX_VALUE;
		double tDistance = 0;
		Direction direction = null;
		for (Direction dir : choices){
			tDistance = to.sub(from.add(dir.toVector())).dist();
			if (tDistance<distanceToTarget){
				distanceToTarget = tDistance;
				direction = dir;
			}
		}
		return direction;
	}

	/**
	 * Moves according to a <i>random walk</i>, used while not hunting in a
	 * {@code MazeSimulation}.
	 * 
	 */

	@Override
	public final Direction move(Direction[] choices) {
		return choices[new Random().nextInt(choices.length)];
	}

	/**
	 * Retrieves the next direction of the animal, by selecting one choice among
	 * the ones available from its position.
	 * <p>
	 * In this variation, the animal knows the world entirely. It can therefore
	 * use the position of other animals in the daedalus to hunt more
	 * effectively.
	 * 
	 * @param choices
	 *            The choices left to the animal at its current position (see
	 *            {@link ch.epfl.maze.physical.World#getChoices(Vector2D)
	 *            World.getChoices(Vector2D)})
	 * @param daedalus
	 *            The world in which the animal moves
	 * @return The next direction of the animal, chosen in {@code choices}
	 */

	abstract public Direction move(Direction[] choices, Daedalus daedalus);

	/**
	 * @param 	Aminal
	 * @param 	From
	 * @return The distance to the animal
	 */
	public static double distanceToAnimal(Animal animal, Vector2D from) {
		return animal.getPosition().sub(from).dist();
	}


	/**
	 * Find the nearest animal
	 * 
	 * @param 	Aminals
	 * @param 	from
	 * @return The nearest animal
	 */
	public static Animal findNearestAnimal(List<Animal> animals, Vector2D from) {
		if (animals.size()==0)
			return null;
		Animal nearestAnimal = null;
		double animalDistance = Double.MAX_VALUE;
		double tDistance = 0;
		if (animals.size() > 0) {
			for (Animal animal : animals) {
				tDistance = distanceToAnimal(animal, from);
				if (tDistance < animalDistance) {
					animalDistance = tDistance;
					nearestAnimal = animal;
				}
			}
		}
		return nearestAnimal;
	}
}
