package ch.epfl.maze.physical;

import java.util.List;
import java.util.ArrayList;

import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * @author Roosembert Palacios
 * @author Thomas Adam
 * 
 *         World that is represented by a labyrinth of tiles in which an
 *         {@code Animal} can move.
 * 
 */

public abstract class World {

	/* tiles constants */
	public static final int FREE = 0;
	public static final int WALL = 1;
	public static final int START = 2;
	public static final int EXIT = 3;
	public static final int NOTHING = -1;

	private int[][] labyrinth;

	/**
	 * Constructs a new world with a labyrinth. The labyrinth must be rectangle.
	 * 
	 * @param labyrinth
	 *            Structure of the labyrinth, an NxM array of tiles
	 */

	public World(int[][] labyrinth) {
		// Create a new local labyrinth
		this.labyrinth = new int[labyrinth.length][labyrinth[0].length];
		// Then copy the labyrinth in input to the local one
		for (int i = 0; i < labyrinth.length; ++i) {
			for (int j = 0; j < labyrinth[0].length; ++j) {
				this.labyrinth[i][j] = labyrinth[i][j];
			}
		}
	}

	/**
	 * Determines whether the labyrinth has been solved by every animal.
	 * 
	 * @return <b>true</b> if no more moves can be made, <b>false</b> otherwise
	 */

	abstract public boolean isSolved();

	/**
	 * Resets the world as when it was instantiated.
	 */

	abstract public void reset();

	/**
	 * Returns a copy of the list of all current animals in the world.
	 * 
	 * @return A list of all animals in the world
	 */

	abstract public List<Animal> getAnimals();

	/**
	 * Checks in a safe way the tile number at position (x, y) in the labyrinth.
	 * 
	 * @param x
	 *            Horizontal coordinate
	 * @param y
	 *            Vertical coordinate
	 * @return The tile number at position (x, y), or the NONE tile if x or y is
	 *         incorrect.
	 */

	public final int getTile(int x, int y) {
		if (((x < getWidth()) && (y < getHeight())) && ((x >= 0) && (y >= 0))) {
			return labyrinth[y][x];
		} else {
			return NOTHING;
		}
	}

	/**
	 * Determines if coordinates are free to walk on.
	 * 
	 * @param x
	 *            Horizontal coordinate
	 * @param y
	 *            Vertical coordinate
	 * @return <b>true</b> if an animal can walk on tile, <b>false</b> otherwise
	 */

	public final boolean isFree(int x, int y) {
		return ((getTile(x, y) == 0) || (getTile(x, y) == 2) || (getTile(x, y) == 3));
	}

	/**
	 * Determines if coordinates are free to walk on.
	 * 
	 * @param position
	 *            A Vector2D position to check
	 * 
	 * @return <b>true</b> if an animal can walk on tile, <b>false</b> otherwise
	 */

	private final boolean isFree(Vector2D position) {
		return isFree(position.getX(), position.getY());
	}

	/**
	 * Computes and returns the available choices for a position in the
	 * labyrinth. The result will be typically used by {@code Animal} in
	 * {@link ch.epfl.maze.physical.Animal#move(Direction[]) move(Direction[])}
	 * 
	 * @param position
	 *            A position in the maze
	 * @return An array of all available choices at a position
	 */

	public final Direction[] getChoices(Vector2D position) {
		// First create a dynamic list and fill it with correct values
		List<Direction> directionList = new ArrayList<Direction>();

		if (isFree(position.addDirectionTo(Direction.UP))) {
			directionList.add(Direction.UP);
		}
		if (isFree(position.addDirectionTo(Direction.DOWN))) {
			directionList.add(Direction.DOWN);
		}
		if (isFree(position.addDirectionTo(Direction.LEFT))) {
			directionList.add(Direction.LEFT);
		}
		if (isFree(position.addDirectionTo(Direction.RIGHT))) {
			directionList.add(Direction.RIGHT);
		}
		if (directionList.size() == 0) {
			directionList.add(Direction.NONE);
		}
		// Then create a good sized array and put values into it
		Direction[] directionArray = new Direction[directionList.size()];
		directionArray = directionList.toArray(directionArray);
		return directionArray;
	}

	/**
	 * Returns horizontal length of labyrinth.
	 * 
	 * @return The horizontal length of the labyrinth
	 */

	public final int getWidth() {
		return labyrinth[0].length;
	}

	/**
	 * Returns vertical length of labyrinth.
	 * 
	 * @return The vertical length of the labyrinth
	 */

	public final int getHeight() {
		return labyrinth.length;
	}

	/**
	 * Returns the entrance of the labyrinth at which animals should begin when
	 * added.
	 * 
	 * @return Start position of the labyrinth, null if none.
	 */

	public final Vector2D getStart() {
		return getCoordinates(START);
	}

	/**
	 * Returns the exit of the labyrinth at which animals should be removed.
	 * 
	 * @return Exit position of the labyrinth, null if none.
	 */

	public final Vector2D getExit() {
		return getCoordinates(EXIT);
	}

	/**
	 * 
	 * Returns the coordinates of the first TILE tile found in the labyrinth.
	 * 
	 * @param TILE
	 *            Tile that is searched
	 * @return Position of the corresponding tile, null if none.
	 */

	private final Vector2D getCoordinates(int TILE) {
		Vector2D position = new Vector2D(-1, -1);
		boolean found = false;
		// Try all possibilities until everything was tried or until the queried
		// position was found
		for (int i = 0; ((i < labyrinth.length)) && !found; ++i) {
			for (int j = 0; ((j < labyrinth[0].length)) && !found; ++j) {
				if (labyrinth[i][j] == TILE) {
					found = true;
					position = new Vector2D(j, i);
				}
			}
		}

		if (found) {
			return position;
		} else {
			return null;
		}
	}
}
