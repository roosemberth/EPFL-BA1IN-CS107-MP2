package ch.epfl.maze.physical;

import java.util.ArrayList;
import java.util.List;

import ch.epfl.maze.util.Vector2D;

/**
 * Daedalus in which predators hunt preys. Once a prey has been caught by a
 * predator, it will be removed from the daedalus.
 * 
 */

public final class Daedalus extends World {
	private List<Predator>	predators		= null;
	private List<Prey>		preys			= null;

	private List<Prey>		allPreys		= null;
	private List<Predator>	allPredators	= null;
	private List<Vector2D>	preysIP			= null;
	private List<Vector2D>	predatorsIP		= null;

	/**
	 * Constructs a Daedalus with a labyrinth structure
	 * 
	 * @param labyrinth
	 *            Structure of the labyrinth, an NxM array of tiles
	 */

	public Daedalus(int[][] labyrinth) {
		super(labyrinth);
		predators = new ArrayList<Predator>();
		preys = new ArrayList<Prey>();
		allPredators = new ArrayList<Predator>();
		allPreys = new ArrayList<Prey>();
		predatorsIP = new ArrayList<Vector2D>();
		preysIP = new ArrayList<Vector2D>();
	}

	@Override
	public boolean isSolved() {
		return preys.isEmpty();
	}

	/**
	 * Adds a predator to the daedalus.
	 * 
	 * @param p
	 *            The predator to add
	 */

	public void addPredator(Predator p) {
		predators.add(p);
		allPredators.add(p);
		predatorsIP.add(new Vector2D(p.getPosition().getX(), p.getPosition().getY()));
	}

	/**
	 * Adds a prey to the daedalus.
	 * 
	 * @param p
	 *            The prey to add
	 */

	public void addPrey(Prey p) {
		preys.add(p);
		allPreys.add(p);
		preysIP.add(new Vector2D(p.getPosition().getX(), p.getPosition().getY()));
	}

	/**
	 * Removes a predator from the daedalus.
	 * 
	 * @param p
	 *            The predator to remove
	 */

	public void removePredator(Predator p) {
		if (hasPredator(p)) {
			predators.remove(p);
		}
	}

	/**
	 * Removes a prey from the daedalus.
	 * 
	 * @param p
	 *            The prey to remove
	 */

	public void removePrey(Prey p) {
		if (hasPrey(p)) {
			preys.remove(p);
		}
	}

	@Override
	public List<Animal> getAnimals() {
		List<Animal> animals = new ArrayList<Animal>();
		animals.addAll(predators);
		animals.addAll(preys);
		return animals;
	}

	/**
	 * Returns a copy of the list of all current predators in the daedalus.
	 * 
	 * @return A list of all predators in the daedalus
	 */

	public List<Predator> getPredators() {
		return new ArrayList<Predator>(predators);
	}

	/**
	 * Returns a copy of the list of all current preys in the daedalus.
	 * 
	 * @return A list of all preys in the daedalus
	 */

	public List<Prey> getPreys() {
		return new ArrayList<Prey>(preys);
	}

	/**
	 * Determines if the daedalus contains a predator.
	 * 
	 * @param p
	 *            The predator in question
	 * @return <b>true</b> if the predator belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasPredator(Predator p) {
		return predators.contains(p);
	}

	/**
	 * Determines if the daedalus contains a prey.
	 * 
	 * @param p
	 *            The prey in question
	 * @return <b>true</b> if the prey belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasPrey(Prey p) {
		return preys.contains(p);
	}

	@Override
	public void reset() {
		for (Predator p : allPredators) {
			p.setPosition(predatorsIP.get(allPredators.indexOf(p)));
		}
		for (Prey p : allPreys) {
			p.setPosition(preysIP.get(allPreys.indexOf(p)));
		}
		predators = new ArrayList<Predator>(allPredators);
		preys = new ArrayList<Prey>(allPreys);
	}
}
