package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Hamster A.I. that remembers the previous choice it has made and the dead ends
 * it has already met.
 * 
 */

public class Hamster extends Animal {
	// Last direction of hamster is stored to lastDirection so that the hamster
	// doesn't take it again
	private Direction lastDirection = Direction.NONE;

	// We store position of impasse into impassePositions so that the hamster
	// never come back here
	private List<Vector2D> impassePositions = new ArrayList<Vector2D>();

	/**
	 * Constructs a hamster with a starting position.
	 * 
	 * @param position
	 *            Starting position of the hamster in the labyrinth
	 */

	public Hamster(Vector2D position) {
		super(position);
	}

	/**
	 * Moves without retracing directly its steps and by avoiding the dead-ends
	 * it learns during its journey.
	 */

	@Override
	public Direction move(Direction[] choices) {
		Direction lastDirectionReversed = lastDirection.reverse();
		Random random = new Random();

		// First we create a list with all the directions that does not
		// conduct to an impasse
		List<Direction> choicesWithoutImpasse = new ArrayList<Direction>();
		boolean isImpasse;
		for (Direction direction : choices) {
			isImpasse = false;
			for (Vector2D position : impassePositions) {
				if (this.getPosition().addDirectionTo(direction).equals(position)) {
					isImpasse = true;
				}
			}
			if (!isImpasse) {
				choicesWithoutImpasse.add(direction);
			}
		}
		// Depending on the size of choicesWithoutImpasse, we're into different
		// situations
		switch (choicesWithoutImpasse.size()) {
		case 0:
			// If all the possible directions conducts to impasses, return NONE
			lastDirection = Direction.NONE;
			break;
		case 1:
			// If there remains only one direction that does not conduct to an
			// impasse, then we're in an impasse. We first need to add the
			// current position to impassePositions, then we set the new
			// direction to be the only one possible
			// We check if current position of hamster and last position that
			// was added to list of impasse positions are next to each other,
			// and if they are, we remove last position that was added to list
			// of impasse positions
			if ((impassePositions.size() > 0)
					&& (this.getPosition().sub(impassePositions.get(impassePositions.size() - 1))
							.toDirection() != Direction.NONE)
					&& (this.getPosition().sub(impassePositions.get(impassePositions.size() - 1)).dist() == 1)) {
				impassePositions.remove(impassePositions.size() - 1);
			}
			impassePositions.add(this.getPosition());
			lastDirection = choicesWithoutImpasse.get(0);
			break;
		default:
			// If all the possible directions does not conduct to impasses, then
			// create an array with all the directions that are not the reverse
			// of active direction (thus, with all legal choices)
			List<Direction> legalChoices = new ArrayList<Direction>();
			for (Direction direction : choicesWithoutImpasse) {
				if (!lastDirectionReversed.equals(direction)) {
					legalChoices.add(direction);
				}
			}
			// Choose a random direction from legal directions array
			lastDirection = legalChoices.get(random.nextInt(legalChoices.size()));
			break;
		}
		return lastDirection;
	}

	@Override
	public Animal copy() {
		return new Hamster(this.getPosition());
	}
}
