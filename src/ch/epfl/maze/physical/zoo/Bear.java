package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Bear A.I. that implements the Pledge Algorithm.
 * 
 */

public class Bear extends Animal {
	// Last direction of the bear is stored to lastDirection
	private Direction lastDirection = Direction.NONE;

	// We store favorite direction of the bear in favoriteDirection;
	private Direction favoriteDirection = Direction.NONE;

	// We count the number of time we turned right, minus the number of time we
	// turned left
	private int counter = 0;

	/**
	 * Constructs a bear with a starting position.
	 * 
	 * @param position
	 *            Starting position of the bear in the labyrinth
	 */

	public Bear(Vector2D position) {
		super(position);
	}

	/**
	 * Moves according to the <i>Pledge Algorithm</i> : the bear tries to move
	 * towards a favorite direction until it hits a wall. In this case, it will
	 * turn right, put its paw on the left wall, count the number of times it
	 * turns right, and subtract to this the number of times it turns left. It
	 * will repeat the procedure when the counter comes to zero, or until it
	 * leaves the maze.
	 */

	@Override
	public Direction move(Direction[] choices) {
		// If the bear doesn't have a favorite direction yet, we randomly give
		// him one between the different possibilities
		if (favoriteDirection == Direction.NONE) {
			Random random = new Random();
			favoriteDirection = choices[random.nextInt(choices.length)];
		}
		List<Direction> choicesArray = new ArrayList<Direction>();
		choicesArray = Arrays.asList(choices);
		// If it is possible to use favorite direction, and if the counter is at
		// 0, we can use favorite direction
		if ((choicesArray.contains(favoriteDirection)) && (counter == 0)) {
			lastDirection = favoriteDirection;
		} else {
			// If counter is at 0, that means that it is not possible to use
			// favorite direction, so we rotate the bear relatively to his
			// right so that he can start following the wall with his left hand
			if (counter == 0) {
				lastDirection = lastDirection.unRelativeDirection(Direction.RIGHT);
				counter++;
			}
			// To follow the wall with his left hand, the bear first check if he
			// can relatively go left, then up, then right, and finally down.
			// When the bear turns left, we decrement the counter, and when he
			// turns right, we increment it
			if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.LEFT))) {
				lastDirection = lastDirection.unRelativeDirection(Direction.LEFT);
				counter--;
			} else if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.UP))) {
				lastDirection = lastDirection.unRelativeDirection(Direction.UP);
			} else if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.RIGHT))) {
				lastDirection = lastDirection.unRelativeDirection(Direction.RIGHT);
				counter++;
			} else {
				lastDirection = lastDirection.unRelativeDirection(Direction.DOWN);
				counter += 2;
			}
		}
		return lastDirection;

	}

	@Override
	public Animal copy() {
		return new Bear(this.getPosition());
	}
}
