package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Space Invader A.I. that implements an algorithm of your choice.
 * <p>
 * Note that this class is considered as a <i>bonus</i>, meaning that you do not
 * have to implement it (see statement: section 6, Extensions libres).
 * <p>
 * If you consider implementing it, you will have bonus points on your grade if
 * it can exit a simply-connected maze, plus additional points if it is more
 * efficient than the animals you had to implement.
 * <p>
 * The way we measure efficiency is made by the test case {@code Competition}.
 * 
 * @see ch.epfl.maze.tests.Competition Competition
 * 
 */

public class SpaceInvader extends Animal {
	// Last direction of the Space Invader is stored to lastDirection to compute
	// relatives directions. We set default last direction as DOWN
	private Direction lastDirection = Direction.DOWN;
	// We store every choice of direction the Space Invader make in
	// chosenDirections to then calculate the best path
	private List<Direction> chosenDirections = new ArrayList<Direction>();
	// We memorize the starting position of the Space Invader. We initially set
	// an impossible value.
	private Vector2D startingPosition = new Vector2D(-1, -1);
	// We memorize if yes or no Space Invader knows the path to exit and
	// has optimized the list of previous directions.
	private boolean knowExitPath = false;
	private boolean hasOptimizedPath = false;
	// We memorize if last relative direction that was chosen is left, to not
	// get stuck if the Space Invader is away from a wall or in the bad
	// direction
	private boolean previousWasLeft = false;
	// That counter will allow us to select the good direction to return from
	// the list of previously chosen directions
	private int followingPathCounter;

	/**
	 * Constructs a space invader with a starting position.
	 * 
	 * @param position
	 *            Starting position of the Space Invader in the labyrinth
	 */

	public SpaceInvader(Vector2D position) {
		super(position);
	}

	/**
	 * Constructs a space invader with a starting position, a list of supposed
	 * previously chosen directions, the supposed previously starting position
	 * of the Space Invader, and two boolean that tells if yes or no the Space
	 * Invader already knows the path to exit and has already optimized the list
	 * of previous directions.
	 * 
	 * @param position
	 *            Starting position of the Space Invader in the labyrinth
	 * @param chosenDirections
	 *            Supposed previously chosen direction of the Space Invader
	 * @param startingPosition
	 *            Supposed previous starting position of the Space Invader
	 * @param knowExitpath
	 *            If yes or no the Space Invader already knows the path to exit
	 * @param hasOptimizedPath
	 *            If yes or no the Space Invader has already optimized the exit
	 *            path
	 */
	public SpaceInvader(Vector2D position, List<Direction> chosenDirections, Vector2D startingPosition,
			boolean knowExitPath, boolean hasOptimizedPath) {
		super(position);
		this.chosenDirections = listCopy(chosenDirections);
		this.startingPosition = new Vector2D(startingPosition.getX(), startingPosition.getY());
		this.knowExitPath = knowExitPath;
		this.hasOptimizedPath = hasOptimizedPath;
	}

	/**
	 * The Space Invader first acts like the monkey and follow the wall with his
	 * left hand. In a list, he memorizes every direction he takes. When the
	 * Space Invader reaches the end, he optimizes that list of directions he
	 * followed, so that this list contain the best path to reach the end (he
	 * removes all the dead-end in that list). Then he simply follows that best
	 * path during all the others computations.
	 */

	@Override
	public Direction move(Direction[] choices) {
		// If stored starting position equals current position, that means
		// that this Space Invader is a copy of previous Space Invader,
		// with a stored list of directions to go to the exit
		if (startingPosition.equals(this.getPosition())) {
			// If at start, we set the counter to follow the known path to 0 so
			// that Space Invader takes the first direction from the known path
			// to go to exit
			followingPathCounter = 0;
			knowExitPath = true;
		}
		// If memorized starting position equals default value, we're at
		// starting position, so we memorize that position
		if (startingPosition.equals(new Vector2D(-1, -1))) {
			startingPosition = new Vector2D(this.getPosition().getX(), this.getPosition().getY());
		}
		// If Space Invader knows the path to exit and has not optimized it yet,
		// we
		// optimize that path
		if (!hasOptimizedPath && knowExitPath) {
			// We remove every dead-end by removing all the opposite direction
			// which are near one-other, browsing through the list. Then we
			// browse the list again, because removing the previous opposite
			// directions can allows us to see new opposite directions. That
			// happens when the Space Invader go to a dead-end: he stores
			// UP-UP-UP-DOWN-DOWN-DOWN, and we remove UP−DOWN to get
			// UP-UP-DOWN-DOWN, then UP-DOWN, then nothing: the dead-end is
			// removed from the list of best directions to take to go to the
			// exit. Because in worst scenario, near all the directions could be
			// a dead-end, we need two these two for loops to achieve best
			// optimization
			for (int j = 0; j < (chosenDirections.size() - 1); ++j) {
				for (int i = 0; i < (chosenDirections.size() - 1); ++i) {
					// If two opposite directions are near one other, they are
					// useless moves, and we remove these directions
					if (((chosenDirections.get(i) == Direction.DOWN) && (chosenDirections.get((i + 1)) == Direction.UP))
							|| ((chosenDirections.get(i) == Direction.UP)
									&& (chosenDirections.get((i + 1)) == Direction.DOWN))
							|| ((chosenDirections.get(i) == Direction.LEFT)
									&& (chosenDirections.get((i + 1)) == Direction.RIGHT))
							|| ((chosenDirections.get(i) == Direction.RIGHT)
									&& (chosenDirections.get((i + 1)) == Direction.LEFT))) {
						// We remove useless moves
						chosenDirections.remove((i + 1));
						chosenDirections.remove(i);
					}
				}
			}
			hasOptimizedPath = true;
		}

		// If the Space Invader knows the path to exit, we make him take it
		if (knowExitPath) {
			// If the counter is too big, that means that true path to go to the
			// exit has not already been found, which can happen if the
			// simulation is reseted before he found the exit.
			// If that happens, we make the Space Invader act like the monkey
			// again for the remaining moves, until he truly finds the exit
			if (chosenDirections.size() == followingPathCounter) {
				followingPathCounter = 0;
				knowExitPath = false;
				hasOptimizedPath = false;
			} else {
				lastDirection = chosenDirections.get(followingPathCounter);
				++followingPathCounter;
			}
		}

		// If the Space Invader doesn't already knows the path to exit, we make
		// him act like the monkey to find it. We store every direction he
		// chooses to list chosenDirection
		if (!knowExitPath) {
			List<Direction> choicesArray = new ArrayList<Direction>();
			choicesArray = Arrays.asList(choices);
			/*
			 * First try to go left relatively, then up, then right, and finally
			 * down. If the Space Invader can relatively go left, we check if
			 * the last relative direction that was chosen is left too, and if
			 * it is, that means that the Space Invader is away from a wall and
			 * is turning around himself, so we make him go relatively right if
			 * he can. If he can't, that means that he's near a wall but in the
			 * opposite direction, so we make him go relatively down. After
			 * every choice of direction, we memorize if that choice was left or
			 * not in previousWasLeft
			 */
			if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.LEFT))) {
				if (previousWasLeft) {
					if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.RIGHT))) {
						lastDirection = lastDirection.unRelativeDirection(Direction.RIGHT);
					} else {
						lastDirection = lastDirection.unRelativeDirection(Direction.DOWN);
					}
					previousWasLeft = false;
				} else {
					lastDirection = lastDirection.unRelativeDirection(Direction.LEFT);
					previousWasLeft = true;
				}
			} else if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.UP))) {
				lastDirection = lastDirection.unRelativeDirection(Direction.UP);
				previousWasLeft = false;
			} else if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.RIGHT))) {
				lastDirection = lastDirection.unRelativeDirection(Direction.RIGHT);
				previousWasLeft = false;
			} else {
				lastDirection = lastDirection.unRelativeDirection(Direction.DOWN);
				previousWasLeft = false;
			}
			chosenDirections.add(lastDirection);
		}
		return lastDirection;
	}

	@Override
	public Animal copy() {
		return new SpaceInvader(this.getPosition(), listCopy(this.chosenDirections),
				new Vector2D(this.startingPosition.getX(), this.startingPosition.getY()), this.knowExitPath,
				this.hasOptimizedPath);
	}

	
	/**
	 * Returns a deep copy of a list of Directions
	 * 
	 * @param listToCopy
	 *            The list to copy
	 */
	private List<Direction> listCopy(List<Direction> listToCopy) {
		List<Direction> newList = new ArrayList<Direction>();
		for (Direction direction : listToCopy) {
			newList.add(direction);
		}
		return listToCopy;
	}
}
