package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Panda A.I. that implements Trémeaux's Algorithm.
 * 
 */
public class Panda extends Animal {
	// Last direction of the Panda is stored to lastDirection
	private Direction lastDirection = Direction.NONE;

	// We store positions of maze into arrays, depending on their color
	private List<Vector2D> colorOne = new ArrayList<Vector2D>();
	private List<Vector2D> colorTwo = new ArrayList<Vector2D>();

	/**
	 * Constructs a panda with a starting position.
	 * 
	 * @param position
	 *            Starting position of the panda in the labyrinth
	 */

	public Panda(Vector2D position) {
		super(position);
	}

	/**
	 * Moves according to <i>Trémeaux's Algorithm</i>: when the panda moves, it
	 * will mark the ground at most two times (with two different colors). It
	 * will prefer taking the least marked paths. Special cases have to be
	 * handled, especially when the panda is at an intersection.
	 */

	@Override
	public Direction move(Direction[] choices) {
		// First we check if only one move is possible
		if (choices.length == 1) {
			// If the only move possible is of color one, we're into an impasse
			// so we assign color two to current position
			if (colorOne.contains(this.getPosition().addDirectionTo(choices[0]))) {
				// Put color to one then to two
				markToOne();
				markToTwo();
			} else {
				// Otherwise we assign color one to current position if it is
				// uncolored, and color two if it is of color one
				markToTwo();
				markToOne();
			}
			// We take the only possible direction
			lastDirection = choices[0];
		} else {
			Random random = new Random();
			// If more than 1 direction is possible, we separate the possible
			// directions by the color of their corresponding position
			List<Direction> nonMarkedDirections = new ArrayList<Direction>();
			List<Direction> colorOneDirections = new ArrayList<Direction>();
			List<Direction> colorTwoDirections = new ArrayList<Direction>();
			for (Direction direction : choices) {
				if (colorOne.contains(this.getPosition().addDirectionTo(direction))) {
					colorOneDirections.add(direction);
				} else if (colorTwo.contains(this.getPosition().addDirectionTo(direction))) {
					colorTwoDirections.add(direction);
				} else {
					nonMarkedDirections.add(direction);
				}
			}

			if (nonMarkedDirections.size() == 0) {
				if (colorOneDirections.size() == 0) {
					// If we can only go to a position of color two, we remove
					// the direction that would make the panda go backward, if
					// exists, then we choose a random direction between the
					// remaining ones. We set the color of current position to
					// one if it is uncolored, and to two if it is of color one
					if (colorTwoDirections.contains(lastDirection.reverse())) {
						colorTwoDirections.remove(lastDirection.reverse());
					}
					markToTwo();
					markToOne();
					lastDirection = colorTwoDirections.get(random.nextInt(colorTwoDirections.size()));
				} else {
					// If we're at an intersection and if all the possible
					// directions conduct to places of color one, we go backward
					if ((choices.length > 2) && (colorOneDirections.size() == choices.length)) {
						lastDirection = lastDirection.reverse();
					}
					// If there are no uncolored position to go to, and if there
					// remains one position of color one, we go to it and we set
					// the color of current position to one if it is uncolored
					// and to two if it is of color one
					else if (colorOneDirections.size() == 1) {
						markToTwo();
						markToOne();
						lastDirection = colorOneDirections.get(0);
					}
					// If there are no uncolored position to go to, and if there
					// are many positions of color one to go to, we go to a
					// random one which is not the last position, and we set the
					// color of current position to one or two. We do not set it
					// to color two if there remains many possible positions of
					// color one to go to and if we're at an intersection
					else {
						if (colorOneDirections.contains(lastDirection.reverse())) {
							colorOneDirections.remove(lastDirection.reverse());
						}
						// If without the previous position, there remains only
						// one possibility of color one, and if the total number
						// of possible choice is two, we're not into an
						// intersection so we can set the color of current
						// position to two if it is currently of color one
						if ((colorOneDirections.size() == 1) && (choices.length == 2)) {
							markToTwo();
						}
						markToOne();
						lastDirection = colorOneDirections.get(random.nextInt(colorOneDirections.size()));
					}
				}
			} else {
				// If there exists directions that conduct the panda to an
				// uncolored position, we randomly choose between them
				markToOne();
				lastDirection = nonMarkedDirections.get(random.nextInt(nonMarkedDirections.size()));
			}
		}
		return lastDirection;

	}

	@Override
	public Animal copy() {
		return new Panda(this.getPosition());
	}

	/**
	 * If it is not already of color one or two, set the color of the current
	 * position of panda to color one
	 */
	private void markToOne() {
		if (!(colorOne.contains(this.getPosition())) && !(colorTwo.contains(this.getPosition()))) {
			colorOne.add(this.getPosition());
		}
	}

	/**
	 * If it is of color one, set the color of the current position of panda to
	 * color two
	 */
	private void markToTwo() {
		if (colorOne.contains(this.getPosition())) {
			colorOne.remove(this.getPosition());
			colorTwo.add(this.getPosition());
		}
	}
}
