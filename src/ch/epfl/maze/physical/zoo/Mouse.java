package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Mouse A.I. that remembers only the previous choice it has made.
 * 
 */

public class Mouse extends Animal {
	// Last direction of the mouse is stored to lastDirection so that the mouse
	// doesn't take it again
	private Direction lastDirection = Direction.NONE;

	/**
	 * Constructs a mouse with a starting position.
	 * 
	 * @param position
	 *            Starting position of the mouse in the labyrinth
	 */

	public Mouse(Vector2D position) {
		super(position);
	}

	/**
	 * Moves according to an improved version of a <i>random walk</i> : the
	 * mouse does not directly retrace its steps.
	 */

	@Override
	public Direction move(Direction[] choices) {
		Direction lastDirectionReversed = lastDirection.reverse();
		Random random = new Random();
		switch (choices.length) {
		case 1:
			// If only one possibility, then takes it
			lastDirection = choices[0];
			break;
		case 2:
			// We separately compute this case for speed improvements, since it
			// happens often. But removing that case to use default case instead
			// would work too.
			// First test if one of the two direction is the reverse of last
			// direction. If it is return the direction which is not the
			// reverse, and if not choose a random direction
			if (choices[0] == lastDirectionReversed) {
				lastDirection = choices[1];
			} else if (choices[1] == lastDirectionReversed) {
				lastDirection = choices[0];
			} else {
				lastDirection = choices[random.nextInt(2)];
			}
			break;
		default:
			// If there are more possible directions, create a new array with
			// all the directions that are not the reverse of active direction,
			// then choose a random one
			List<Direction> legalChoices = new ArrayList<Direction>();
			for (Direction direction : choices) {
				if (!lastDirectionReversed.equals(direction)) {
					legalChoices.add(direction);
				}
			}
			// Return a random value from legal possible choices
			lastDirection = legalChoices.get(random.nextInt(legalChoices.size()));
			break;
		}
		return lastDirection;
	}

	@Override
	public Animal copy() {
		return new Mouse(this.getPosition());
	}
}
