package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Monkey A.I. that puts its hand on the left wall and follows it.
 * 
 */

public class Monkey extends Animal {
	// Last direction of the monkey is stored to lastDirection to compute
	// relatives directions. We set default last direction as DOWN
	private Direction lastDirection = Direction.DOWN;

	// We memorize if last relative direction that was chosen is left, to not
	// get stuck if the monkey is away from a wall or in the bad direction
	private boolean previousWasLeft = false;

	/**
	 * Constructs a monkey with a starting position.
	 * 
	 * @param position
	 *            Starting position of the monkey in the labyrinth
	 */

	public Monkey(Vector2D position) {
		super(position);
	}

	/**
	 * Moves according to the relative left wall that the monkey has to follow.
	 */

	@Override
	public Direction move(Direction[] choices) {
		List<Direction> choicesArray = new ArrayList<Direction>();
		choicesArray = Arrays.asList(choices);
		// First try to go left relatively, then up, then right, and finally
		// down. If the monkey can relatively go left, we check if the last
		// relative direction that was chosen is left too, and if it is, that
		// means that the monkey is away from a wall and is turning around
		// himself, so we make him go relatively right if he can. If he can't,
		// that means that he's near a wall but in the opposite direction, so we
		// make him go relatively down
		// After every choice of direction, we memorize if that choice was left
		// or not in previousWasLeft
		if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.LEFT))) {
			if (previousWasLeft) {
				if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.RIGHT))) {
					lastDirection = lastDirection.unRelativeDirection(Direction.RIGHT);
				} else {
					lastDirection = lastDirection.unRelativeDirection(Direction.DOWN);
				}
				previousWasLeft = false;
			} else {
				lastDirection = lastDirection.unRelativeDirection(Direction.LEFT);
				previousWasLeft = true;
			}
		} else if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.UP))) {
			lastDirection = lastDirection.unRelativeDirection(Direction.UP);
			previousWasLeft = false;
		} else if (choicesArray.contains(lastDirection.unRelativeDirection(Direction.RIGHT))) {
			lastDirection = lastDirection.unRelativeDirection(Direction.RIGHT);
			previousWasLeft = false;
		} else {
			lastDirection = lastDirection.unRelativeDirection(Direction.DOWN);
			previousWasLeft = false;
		}
		return lastDirection;
	}

	@Override
	public Animal copy() {
		return new Monkey(this.getPosition());
	}
}
